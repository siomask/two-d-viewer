const editor = require('fabric').fabric;
let id = 0;
editor.Canvas.prototype.setBackgroundImage = function(url) {
  return new Promise((resolve, reject) => {
    this.clear();
    const self = this;
    editor.Image.fromURL(url, function(image) {
      image.set({
        lockMovementX: true,
        lockMovementY: true,
        lockRotation: true,
        selectable: false,
        hasBorders: false,
        hasControls: false,
        id: id++
      });
      self.add(image);
      self.requestRenderAll();
      image.fitToParent();
      resolve(image);
    });
  });
};

editor.Image.prototype.fitToParent = function(delta = 0.9) {
  const image = this;
  const self = this;
  const mnf_self_width = self.canvas.width;
  const mnf_self_height = self.canvas.height;
  if (image.width / image.height > 1) {
    image.scaleToWidth(mnf_self_width * delta);
  } else {
    image.scaleToHeight(mnf_self_height * delta);
  }
  image.center();
  self.canvas.requestRenderAll();
};

editor.Canvas.prototype.init = async function(opt) {
  const { imgUrl } = opt;
  await this.setBackgroundImage(imgUrl);
};
editor.Canvas.prototype.destroy = function(opt) {
  this.dispose();
  this.clear();
};
editor.Canvas.prototype.addText = function(opt) {
  const text = this.createText(opt);
  this.add(text);
  text.center();
  this.setActiveObject(text);
  this.refresh();
};
editor.Canvas.prototype.editText = function(opt) {
  const old = this.findById(opt.id);
  this.remove(old);
  const text = this.createText({
    ...old,
    ...opt
  });
  text.id = old.id;
  this.add(text);
  this.setActiveObject(text);
  this.refresh();
};
editor.Canvas.prototype.findById = function(id) {
  return this.getObjects().find(el => el.id === id);
};
editor.Canvas.prototype.createText = function(opt) {
  return new fabric.Text(opt.text, {
    ...opt,
    underline: opt.underline || false,
    fill: opt.fill || 'black',
    fontFamily: opt.fontFamily || 'Delicious_500',
    fontSize: opt.fontSize || 80,
    fontStyle: opt.fontStyle || 'normal',
    textAlign: opt.textAlign || 'left',
    id: id++
  });
};
editor.Canvas.prototype.addTextInBox = function(opt) {
  const self = this;
  const square = new fabric.Rect({
    width: self.width * 0.5,
    height: self.height * 0.5,
    fill: '#fff',
    lockMovementX: true,
    lockMovementY: true,
    lockRotation: true,
    selectable: false,
    hasBorders: false,
    hasControls: false,
    id: id++
  });
  const text = this.createText(opt);
  text.set({
    left: square.width / 2 - text.width / 2,
    top: square.height / 2 - text.height / 2,
    lockMovementX: true,
    lockMovementY: true,
    lockRotation: true,
    selectable: false,
    hasBorders: false,
    hasControls: false
  });
  const g = new fabric.Group([square, text], {});
  g.hasControls = true;
  self.add(g);
  g.center();
  this.refresh();
};

editor.Canvas.prototype.onRemove = function(item) {
  this.remove(this.findById(item.id));
  this.renderAll();
};
editor.Canvas.prototype.addImage = function(opt) {
  return new Promise((resolve, reject) => {
    const self = this;
    editor.Image.fromURL(opt.url, function(image) {
      self.add(image);
      image.set({
        id: id++
      });
      self.setActiveObject(image);
      image.fitToParent(0.4);
      self.requestRenderAll();
      resolve(image);
    });
  });
};
editor.Canvas.prototype.refresh = function(opt) {
  this.requestRenderAll();
};

module.exports = editor;
