(function() {
  const canvas = (window.__Canvas = new fabric.Canvas("c", {
    appParentContainer: "canvasContainer"
  }));

  // canvas.startEvents();
  // canvas.setCanvas(60, 40, 'cm');

  canvas.setWidth(710);
  canvas.setHeight(610);
  canvas.init({ imgUrl: "/source.jpg" });
})();
