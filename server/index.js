const express = require("express"),
  path = require("path"),
  app = express();

app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*"); //allow request from anywhere, not safe, replace * with your domain name
  res.setHeader("Access-Control-Allow-Methods", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

app.use(express.static("public/dist"));


var server = app.listen(3003, function() {
  console.log("app running on port.", server.address().port, server.address());
});
